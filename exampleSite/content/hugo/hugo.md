---
title: "Hugo"
date: 2017-09-04T15:19:43+02:00
draft: false
---

Built with <a href="http://gohugo.io">Hugo</a> and the
<a href="https://github.com/crakjie/landing-page-hugo">landing-page-hugo</a>
theme.